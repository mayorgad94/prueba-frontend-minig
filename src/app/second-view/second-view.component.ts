import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../service/services.service';
import { OrderPipe } from 'ngx-order-pipe';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-second-view',
  templateUrl: './second-view.component.html',
  styleUrls: ['./second-view.component.css']
})
export class SecondViewComponent implements OnInit {

  show: boolean = false;
  showButton: boolean = true;
  array= [];

  constructor(private api: ServicesService,private orderPipe: OrderPipe,private spinner: NgxSpinnerService) { 

  }

  ngOnInit() {
  }

  buttonGetData(){
    this.showButton = false;
    this.spinner.show();
    this.getData();
  }

  getData(){
    this.api.second().subscribe((res :any)=>{
      this.spinner.hide();
        if(res.data == null){
          alert('El servicio retorno 0 registros, se recargara la página automáticamente');
          window.location.reload();
        }else{
          console.log(res);
          this.show = true;
          this.array = res.data;
        }
    }, error => {
      alert('Existieron errores al obtener los datos, recargue la página');
    }); 
  }
  getCurrency(data,leter){
    var text = data.paragraph;
    text = text.toLowerCase();
    var count =  text.split(leter).length - 1;
    return count;
  }
  getTotal(data){

    var count = 0;
    
    var min=["a","b","c","d","e","f","g","h","i","j","k","l","m","n","ñ","o","p","q","r","s","t","u","v","w","x","y","z"];
    for(var i = 0; i < 27; i++){
      count = count + this.getCurrency(data,min[i]);
    }
    return count;
  }
}
