import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FirstViewComponent } from './first-view/first-view.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { SecondViewComponent } from './second-view/second-view.component';
import { HttpClientModule } from '@angular/common/http';
import { ServicesService } from './service/services.service';
import { OrderModule } from 'ngx-order-pipe';
import { NgxSpinnerModule } from "ngx-spinner";

const appRoutes: Routes = [
  { path: '', component: FirstViewComponent },
  { path: 'second-view', component: SecondViewComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    FirstViewComponent,
    NavBarComponent,
    SecondViewComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    OrderModule,
    NgxSpinnerModule
  ],
  providers: [ServicesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
