import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {

  constructor(private http: HttpClient) { }

  url = 'http://168.232.165.184/prueba';

  first(){
    return this.http.get(this.url+'/array');
  }
  second(){
    return this.http.get(this.url+'/dict');
  }
}
