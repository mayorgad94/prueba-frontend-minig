import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../service/services.service';
import { OrderPipe } from 'ngx-order-pipe';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-first-view',
  templateUrl: './first-view.component.html',
  styleUrls: ['./first-view.component.css']
})
export class FirstViewComponent implements OnInit {

  array= [];
  arrayFilter = [];
  order: string = 'number';
  show: boolean = false;
  numbersOrder:string = '';
  showButton: boolean = true;

  constructor(private api: ServicesService,private orderPipe: OrderPipe,private spinner: NgxSpinnerService) { 
    
  }

  ngOnInit() {
  }

  buttonGetData(){
    this.showButton = false;
    this.spinner.show();
    this.getData();
  }

  getData(){
    this.api.first().subscribe((res :any)=>{
      this.spinner.hide();
        if(res.data == null){
          alert('El servicio retorno 0 registros, se recargara la página automáticamente');
          window.location.reload();
        }else{
          this.show = true;
          this.array = res.data;
          this.filterData(res.data);
        }
    }, error => {
      alert('Existieron errores al obtener los datos, recargue la página');
    }); 
  }

  filterData(data){

    var contador;
    var first;
    var last;
    var arrayDistinct = data.filter((x, i, a) => a.indexOf(x) == i);

    for( var i = 0; i < arrayDistinct.length ; i++){
      contador = 0;
      for( var x = 0; x < data.length ; x++){
        if(arrayDistinct[i] == data[x]){
          contador = contador +1;
        }
      }
      first = this.getFirst(data,arrayDistinct[i]);
      last = this.getLast(data,arrayDistinct[i]);
      this.arrayFilter[i] = {
        item:i+1,
        number: arrayDistinct[i],
        quantity: contador,
        first: first,
        last: last
      };
    }
    this.concatNumbers(this.orderPipe.transform(this.arrayFilter, this.order));
  }

  concatNumbers(arrayDistinct){
    console.log(arrayDistinct);
    for(var i = 0; i < arrayDistinct.length ; i++){
      this.numbersOrder = this.numbersOrder +' '+arrayDistinct[i].number;
    }
    console.log(this.numbersOrder);
  }

  getFirst(array,number){
    for(var i = 0; i < array.length ; i++){
      if(array[i] == number){
        return i;
      }
    }
  }

  getLast(array,number){
    for(var i = array.length; i >= 0 ; i--){
      if(array[i] == number){
        return i;
      }
    }
  }

}
